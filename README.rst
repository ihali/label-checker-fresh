Fresh Label Checker
===================

This module contains a class for label detection and validation. The Label Checker performs the following tasks:


Label Detection: Detects labels in an image  of Fresh Food Package.
Label Validation: Utilizes OCR to extract and interpret text from the label. Furthermore it validates the content
of the detected label, ensuring it meets predefined criteria (such as correct label and expiry).
Angle Detection: Detects and validates the angle of the label relative to the box.


Docker
------

For more controlled deployments and to get rid of "works on my computer" -syndrome, we always
make sure our software works under docker.

It's also a quick way to get started with a standard development environment.

SSH agent forwarding
^^^^^^^^^^^^^^^^^^^^

We need buildkit_::

    export DOCKER_BUILDKIT=1

.. _buildkit: https://docs.docker.com/develop/develop-images/build_enhancements/

And also the exact way for forwarding agent to running instance is different on OSX::

    export DOCKER_SSHAGENT="-v /run/host-services/ssh-auth.sock:/run/host-services/ssh-auth.sock -e SSH_AUTH_SOCK=/run/host-services/ssh-auth.sock"

and Linux::

    export DOCKER_SSHAGENT="-v $SSH_AUTH_SOCK:$SSH_AUTH_SOCK -e SSH_AUTH_SOCK"

Creating a development container
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Build image, create container and start it (switch the 58328 port to the port from src/labelchecker/defaultconfig.py)::

    docker build --progress plain --ssh default --target devel_shell -t labelchecker:devel_shell .
    docker create --name labelchecker_devel -p 58328:58328 -p 58329:58329 -v `pwd`":/app" -it -v /tmp:/tmp `echo $DOCKER_SSHAGENT` labelchecker:devel_shell
    docker start -i labelchecker_devel

pre-commit considerations
^^^^^^^^^^^^^^^^^^^^^^^^^

If working in Docker instead of native env you need to run the pre-commit checks in docker too::

    docker exec -i labelchecker_devel /bin/bash -c "pre-commit install"
    docker exec -i labelchecker_devel /bin/bash -c "pre-commit run --all-files"

You need to have the container running, see above. Or alternatively use the docker run syntax but using
the running container is faster::

    docker run -it --rm -v `pwd`":/app" labelchecker:devel_shell -c "pre-commit run --all-files"

Test suite
^^^^^^^^^^

You can use the devel shell to run py.test when doing development, for CI use
the "tox" target in the Dockerfile::

    docker build --ssh default --target test -t labelchecker:test .
    docker run -it --rm -v `pwd`":/app" `echo $DOCKER_SSHAGENT` labelchecker:test

Production docker
^^^^^^^^^^^^^^^^^

There's a "production" target as well for running the application and remember to change that architecture tag to arm64 if building on ARM::

    docker build --progress plain --ssh default --target production -t labelchecker:amd64-latest .
    docker run --rm -it --name labelchecker -v `pwd`/config.toml:/app/docker_config.toml -p 58328:58328 -p 58329:58329 -v /tmp:/tmp `echo $DOCKER_SSHAGENT` labelchecker:amd64-latest


Local Development
-----------------

TODO: Remove the repo init from this document after you have done it.

TLDR:

- Create and activate a Python 3.9 virtualenv (assuming virtualenvwrapper)::

    mkvirtualenv -p `which python3.9` my_virtualenv

- change to a branch::

    git checkout -b my_branch

- install Poetry: https://python-poetry.org/docs/#installation
- Install project deps and pre-commit hooks::

    poetry install
    pre-commit install
    pre-commit run --all-files

- Ready to go, try the following::

    labelchecker --defaultconfig > config.toml

Remember to activate your virtualenv whenever working on the repo, this is needed
because pylint and mypy pre-commit hooks use the "system" python for now (because reasons).

Running "pre-commit run --all-files" and "py.test -v" regularly during development and
especially before committing will save you some headache.

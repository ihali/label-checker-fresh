"""Test CLI scripts"""
from labelchecker.console import dump_default_config
from labelchecker import DEFAULT_CONFIG_STR


def test_default_config_func(capsys):  # type: ignore
    """Make sure the default config is/is not dumped"""
    dump_default_config(None, None, True)
    captured = capsys.readouterr()
    assert captured.out.strip() == DEFAULT_CONFIG_STR.strip()
    dump_default_config(None, None, False)
    captured = capsys.readouterr()
    assert captured.out.strip() == ""

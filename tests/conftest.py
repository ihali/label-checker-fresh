"""
`conftest.py`: Pytest configuration and fixtures for the project.
"""
import pytest
from labelchecker import LabelChecker


@pytest.fixture(scope="session")
def labelchecker_instance():
    """Fixture to provide the class instance."""
    instance = LabelChecker()
    return instance

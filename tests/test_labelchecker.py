"""Basic filler tests"""
import cv2
import numpy as np
import pytest
from labelchecker import LabelChecker, LabelStatus


def create_dummy_image():
    """
    Create a dummy image for testing.
    """

    # Image dimensions
    image_width = 1400
    image_height = 1400

    # Box dimensions and position
    box_center_xy_px = [800, 600]
    box_dimensions_width_height_px = [800, 800]

    # Vertical strip dimensions
    strip_width = 300

    # Create a black background image
    image = np.ones((image_height, image_width, 3), dtype=np.uint8) * 255

    # Calculate box coordinates
    box_left = box_center_xy_px[0] - box_dimensions_width_height_px[0] // 2
    box_upper = box_center_xy_px[1] - box_dimensions_width_height_px[1] // 2
    box_right = box_center_xy_px[0] + box_dimensions_width_height_px[0] // 2
    box_lower = box_center_xy_px[1] + box_dimensions_width_height_px[1] // 2

    # Draw white box
    cv2.rectangle(
        image,
        (box_left, box_upper),
        (box_right, box_lower),
        (150, 240, 20),
        thickness=cv2.FILLED,
    )

    # Calculate strip coordinates
    strip_left = box_center_xy_px[0] - strip_width // 2
    strip_upper = box_upper + 50
    strip_right = box_center_xy_px[0] + strip_width // 2
    strip_lower = box_lower - 50

    # Draw green vertical strip
    cv2.rectangle(
        image,
        (strip_left, strip_upper),
        (strip_right, strip_lower),
        (200, 120, 69),
        thickness=cv2.FILLED,
    )

    # Add text to the strip
    text1 = "OCR"
    text2 = "TEST"

    font = cv2.FONT_HERSHEY_SIMPLEX
    text_scale = 1
    text_thickness = 2
    text_size = cv2.getTextSize(text1, font, text_scale, text_thickness)[0]
    text_x = (strip_left + strip_right - text_size[0]) // 2
    text_y = (strip_upper + strip_lower + text_size[1]) // 2
    cv2.putText(
        image,
        text1,
        (text_x - 50, text_y - 200),
        font,
        text_scale,
        (0, 0, 0),
        thickness=text_thickness,
    )
    cv2.putText(
        image,
        text2,
        (text_x + 50, text_y + 200),
        font,
        text_scale,
        (0, 0, 0),
        thickness=text_thickness,
    )

    return image


@pytest.mark.asyncio
async def test_set_roi_interest():
    """
    Test setting Region of Interest (ROI) on an image.
    """
    image = np.zeros((200, 400, 3), dtype=np.uint8)
    roi = [100, 50, 200, 100]  # (x, y, width, height)
    roi_image = await LabelChecker.set_roi_interest_async(image, roi)
    assert roi_image.shape[:2] == (100, 200)


def test_initialize_label_checker(labelchecker_instance):
    """
    Test initialization of LabelChecker instance.
    """
    assert isinstance(labelchecker_instance, LabelChecker)


@pytest.mark.asyncio
async def test_run_async(labelchecker_instance):
    """
    Test asynchronous label checking.
    """
    dummy_image = create_dummy_image()

    label_status, visualized_results = await labelchecker_instance.run_async(
        dummy_image
    )

    assert isinstance(label_status, LabelStatus)
    assert isinstance(visualized_results, np.ndarray)

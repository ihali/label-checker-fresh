"""Default configuration file contents"""

# Remember to add tests for keys into test_python_ocr.py
DEFAULT_CONFIG_STR = """

[preprocessing]
    roi = []
    denoise = false
    equalize_histogram = false
    tile_grid_size = 16

[label_detection]
    use_gpu = false
    model_type = "vit_t"
    model_path = "./weights/mobile_sam.pt"

[ocr_configuration]
    language = "en"
    detection = true
    recognition = true
    use_gpu = false
    gpu_mem = 2000
    roi = []
    resize_ratio = 1
    denoise = false
    equalize_histogram = true
    tile_grid_size = 16
    binarize = false
    text_match_threshold = 0.8

[box_label_specs]
    box_size_wh_mm = [180, 165]
    box_center_xy_px = [800, 600]
    box_dimenstions_xy_px = [800, 800]
    keywords = ["hetki", "suosikkisalaatti", "leipajuusto","fresh"]
    expiry_date = "31.03.2024"
    datetime_format = "DD-MM-YYYY"
    label_tilt_tolerance_deg = 5.0
    label_centre_offset_tolerance_mm = 20

""".lstrip()

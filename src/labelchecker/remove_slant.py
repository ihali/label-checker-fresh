import cv2
import numpy as np

# Read the image
image = cv2.imread('./Examples/hetki_brown.jpg')

import cv2
import numpy as np

# Read the image

# Define the coordinates of the line (you may need to adjust these)

# Define the coordinates of the line (you may need to adjust these)
point1 = (50, 100)  # (x1, y1)
point2 = (200, 300)  # (x2, y2)

# Get the dimensions of the image
height, width = image.shape[:2]

# Create a meshgrid of x and y coordinates
x_coords, y_coords = np.meshgrid(np.arange(width), np.arange(height))

# Compute the position of each pixel relative to the line
pos = (x_coords - point1[0]) * (point2[1] - point1[1]) - (y_coords - point1[1]) * (point2[0] - point1[0])

# Create a mask to keep pixels on one side of the line
one_side_mask = np.zeros_like(pos, dtype=np.uint8)
one_side_mask[pos > 0] = 255

# Invert the mask
one_side_mask = cv2.bitwise_not(one_side_mask)

# Apply the mask to the image
result = cv2.bitwise_and(image, image, mask=one_side_mask)

# Show the result
cv2.imshow('Result', result)
cv2.waitKey(0)
cv2.destroyAllWindows()

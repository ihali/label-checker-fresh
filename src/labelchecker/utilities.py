""" Helper functions for the module """
from typing import List, Tuple, Any
import logging

import numpy as np
import cv2
from sklearn.cluster import KMeans

logging.basicConfig(level=logging.INFO)


def get_segmentation_seeds_for_box(
    image_shape: Tuple[int, int],
    box_center: Tuple[int, int],
    box_width: int,
    box_height: int,
    num_points: int = 250,
    foreground: bool = True,
) -> Tuple[np.ndarray, List[int]]:
    """
    Generate segmentation seeds for a box region.

    Args:
        image_shape (Tuple[int, int]): Shape of the image.
        box_center (Tuple[int, int]): Center coordinates of the box.
        box_width (int): Width of the box.
        box_height (int): Height of the box.
        num_points (int, optional): Number of points to generate. Defaults to 250.
        foreground (bool, optional): Whether to segment the foreground or background. Defaults to True.

    Returns:
        Tuple[np.ndarray, List[int]]: Segmentation points and their corresponding classes.
    """

    def create_rectangular_mask(
        image_shape: Tuple[int, int], center: Tuple[int, int], width: int, height: int
    ) -> np.ndarray:
        """Create a rectangular mask."""
        mask = np.zeros(image_shape[:2], dtype=np.uint8)
        x_coord, y_coord = center
        x_1, y_1 = max(0, x_coord - width // 2), max(0, y_coord - height // 2)
        x_2, y_2 = (
            min(image_shape[1] - 1, x_coord + width // 2),
            min(image_shape[0] - 1, y_coord + height // 2),
        )
        mask[y_1:y_2, x_1:x_2] = 255
        return mask

    def generate_points(image_shape: Tuple[int, int], num_points: int) -> np.ndarray:
        """Generate points within the image."""
        height, width = image_shape[:2]
        x_coords = np.linspace(0, width - 1, int(np.sqrt(num_points)))
        y_coords = np.linspace(0, height - 1, int(np.sqrt(num_points)))
        x_val, y_val = np.meshgrid(x_coords, y_coords)
        points = np.column_stack((x_val.ravel().astype(int), y_val.ravel().astype(int)))
        return points

    def filter_points(
        points: np.ndarray, mask: np.ndarray
    ) -> Tuple[np.ndarray, np.ndarray]:
        """Filter points based on mask."""
        points_inside_mask = np.array(
            [point for point in points if mask[point[1], point[0]] > 0]
        )
        points_outside_mask = np.array(
            [point for point in points if mask[point[1], point[0]] == 0]
        )
        return points_inside_mask, points_outside_mask

    points = generate_points(image_shape, num_points)
    mask = create_rectangular_mask(image_shape, box_center, box_width, box_height)
    points_inside_mask, points_outside_mask = filter_points(points, mask)
    box_segmentation_points = np.vstack((points_inside_mask, points_outside_mask))
    if foreground:
        box_segmentation_classes = [1] * len(points_inside_mask) + [0] * len(
            points_outside_mask
        )
    else:
        box_segmentation_classes = [0] * len(points_inside_mask) + [1] * len(
            points_outside_mask
        )
    return box_segmentation_points, box_segmentation_classes


def detect_vertical_lines(
    mask: np.ndarray,
    min_angle: int = 60,
    max_angle: int = 120,
    std_threshold: float = 1.5,
) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """
    Detect vertical lines in a mask.

    Args:
        mask (np.ndarray): Input mask image.
        min_angle (int, optional): Minimum angle threshold. Defaults to 60.
        max_angle (int, optional): Maximum angle threshold. Defaults to 120.
        std_threshold (float, optional): Standard deviation threshold for filtering outliers. Defaults to 1.5.

    Returns:
        Filtered lines, their angles, and the line image.
    """

    label_edges = cv2.Canny(mask, 50, 150, apertureSize=3)

    # Detect lines using Hough transform
    lines = cv2.HoughLinesP(
        label_edges,
        1,
        0.1 * np.pi / 180,
        threshold=100,
        minLineLength=30,
        maxLineGap=30,
    )

    if lines is not None:
        # Filter lines by angle
        filtered_lines = []
        filtered_angles = []
        for line in lines:
            x_1, y_1, x_2, y_2 = line[0]
            angle = (
                np.arctan2(y_2 - y_1, x_2 - x_1) * 180 / np.pi
            )  # Calculate angle in degrees
            if min_angle <= abs(angle) <= max_angle:
                filtered_lines.append(line)
                filtered_angles.append(abs(angle))

        filtered_angles_arr = np.array(filtered_angles)
        filtered_lines_arr = np.array(filtered_lines)

        # Filter outlier small opposing lines
        z_scores = np.abs(
            (filtered_angles_arr - np.mean(filtered_angles_arr))
            / np.std(filtered_angles_arr)
        )
        inliers = np.where(z_scores <= std_threshold)[0]

        if len(inliers) > 0:
            filtered_angles_arr = filtered_angles_arr[inliers]
            filtered_lines_arr = filtered_lines_arr[inliers, :, :]

        # Draw each filtered line
        line_image = np.zeros_like(label_edges)
        for line in filtered_lines_arr:
            color = 255
            x_1, y_1, x_2, y_2 = line[0]
            line_image = cv2.line(line_image, (x_1, y_1), (x_2, y_2), color, 2)  # type: ignore

    else:
        logging.warning("No lines detected in the image.")
        return np.empty((0,)), np.empty((0,)), np.empty((0,))

    return np.squeeze(filtered_lines_arr, axis=1), filtered_angles_arr, line_image


async def cluster_lines(
    hough_lines: np.ndarray, n_clusters: int = 2
) -> tuple[np.ndarray, np.ndarray]:
    """Cluster lines as groups."""

    # Clustering the lines into left and right clusters
    x_values = hough_lines[:, [0, 2]]
    if np.sum(np.std(x_values[:], axis=0)) < 40:  # probably one cluster only exists
        return np.empty((0,)), np.empty((0,))
    kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(x_values)

    # Separate lines into left and right clusters
    line_type_1 = []
    line_type_2 = []
    for i, line in enumerate(hough_lines):
        if kmeans.labels_[i] == 0:
            line_type_1.append(line)
        else:
            line_type_2.append(line)

    line_type_1_arr = np.array(line_type_1)
    line_type_2_arr = np.array(line_type_2)

    if np.mean(line_type_1_arr[:, 0]) < np.mean(line_type_2_arr[:, 0]):
        left_lines = line_type_1_arr
        right_lines = line_type_2_arr
    else:
        left_lines = line_type_2_arr
        right_lines = line_type_1_arr

    return left_lines, right_lines


async def combine_linelets(
    left_lines: np.ndarray, right_lines: np.ndarray
) -> tuple[np.ndarray, np.ndarray]:
    """Combine linelets into a single line."""

    def fit_line(lines: np.ndarray) -> np.ndarray:
        """Fit a line over saveral lines."""
        slopes = (lines[:, 3] - lines[:, 1]) / (lines[:, 2] - lines[:, 0])

        # Check if all lines are vertical (have infinite slope)
        if np.all(np.isinf(slopes)):
            # If all lines are vertical, find average x-coordinate and min/max y-coordinates
            avg_x = np.average(lines[:, 0])
            y_min = lines[:, 1].min()
            y_max = lines[:, 3].max()
            return np.array([int(avg_x), y_min, int(avg_x), y_max])

        # Handling lines with finite slopes
        non_vertical_lines = np.logical_not(np.isinf(slopes))
        average_slope = np.average(slopes[non_vertical_lines])
        intercepts = lines[:, 1] - average_slope * lines[:, 0]
        average_intercept = np.average(intercepts[non_vertical_lines])
        x_min = lines[non_vertical_lines, 0].min()
        x_max = lines[non_vertical_lines, 2].max()
        y_min = int(average_slope * x_min + average_intercept)
        y_max = int(average_slope * x_max + average_intercept)
        line_out = [x_min, y_min, x_max, y_max]
        line_out = (
            [line_out[2], line_out[3], line_out[0], line_out[1]]
            if line_out[1] > line_out[3]
            else line_out
        )

        return np.array(line_out)

    combined_left = (
        np.empty((0,)) if is_array_empty(left_lines) else fit_line(left_lines)
    )
    combined_right = (
        np.empty((0,)) if is_array_empty(right_lines) else fit_line(right_lines)
    )

    return combined_left, combined_right


async def rotate_line_upright_async(line: np.ndarray) -> np.ndarray:
    """Rotate lines around mid-point to be upright."""

    if is_array_empty(line):
        return line

    x_1, y_1, x_2, y_2 = line
    if x_1 == x_2:
        print("The line is already vertical.")
        return line

    # Calculate slope
    slope = (y_2 - y_1) / (x_2 - x_1)

    # Calculate midpoint
    midpoint_x = (x_1 + x_2) / 2
    midpoint_y = (y_1 + y_2) / 2

    # Set both x-coordinates to midpoint x
    new_x1 = midpoint_x
    new_x2 = midpoint_x

    # Calculate new y-coordinates
    new_y1 = midpoint_y - ((x_1 - midpoint_x) * slope)
    new_y2 = midpoint_y + ((midpoint_x - x_2) * slope)

    return np.array((new_x1, new_y1, new_x2, new_y2)).astype(int)


async def remove_mask_to_side_of_line(
    line: np.ndarray, mask: np.ndarray, side: str = "Left"
) -> np.ndarray:
    """Remove mask on the side of edges"""

    if is_array_empty(line):
        return mask

    x_1, y_1, x_2, y_2 = line
    # Get the dimensions of the image
    height, width = mask.shape[:2]

    # Handle vertical line separately
    if x_1 == x_2:
        mask_out = mask.copy()
        if side.lower() in ["left", "l"]:
            mask_out[:, :x_1] = 0
        else:
            mask_out[:, x_1:] = 0
        return mask_out

    # Compute line parameters A, B, and C
    const_a = y_2 - y_1
    const_b = x_1 - x_2
    const_c = x_2 * y_1 - x_1 * y_2

    # Create a meshgrid of x and y coordinates
    x_coords, y_coords = np.meshgrid(np.arange(width), np.arange(height))

    # Compute the position of each pixel relative to the line
    pos = const_a * x_coords + const_b * y_coords + const_c

    # Create a mask to keep pixels on one side of the line
    one_side_mask = np.zeros_like(pos, dtype=np.uint8)
    if side.lower() in ["left", "l"]:
        one_side_mask[pos <= 0] = 255
    else:
        one_side_mask[pos >= 0] = 255

    # Invert the mask
    inverted_one_side_mask = cv2.bitwise_not(one_side_mask)

    # Apply the mask to the image
    result = cv2.bitwise_and(mask, mask, mask=inverted_one_side_mask)
    return result


def morph_close(
    mask: np.ndarray, kernel_size: Tuple[int, int] = (10, 10)
) -> np.ndarray:
    """
    Perform morphological closing operation on the mask.

    Args:
        mask (np.ndarray): Input mask image.
        kernel_size (Tuple[int, int]): Size of the kernel.

    Returns:
        np.ndarray: Morphologically closed mask.
    """
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, [3, 3])
    opened = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, kernel_size)
    return cv2.morphologyEx(opened, cv2.MORPH_CLOSE, kernel)


def morph_open(mask: np.ndarray, kernel_size: Tuple[int, int] = (5, 5)) -> np.ndarray:
    """
    Perform morphological opening operation on the mask.

    Args:
        mask (np.ndarray): Input mask image.
        kernel_size (Tuple[int, int]): Size of the kernel.

    Returns:
        np.ndarray: Morphologically opened mask.
    """
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, kernel_size)
    return cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)


def get_largest_body(mask: np.ndarray, approximate: bool = False) -> np.ndarray:
    """
    Get the largest body from the mask.

    Args:
        mask (np.ndarray): Input mask image.
        approximate (bool, optional): Whether to approximate contours. Defaults to False.

    Returns:
        np.ndarray: Mask of the largest body.
    """
    cnts, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    cnt = max(cnts, key=cv2.contourArea)

    # Approximate contours to straight lines
    if approximate:
        epsilon = 0.06 * cv2.arcLength(cnt, True)
        cnt = cv2.approxPolyDP(cnt, epsilon, True)

    # Output
    output = np.zeros(mask.shape, np.uint8)
    output = cv2.drawContours(output, [cnt], -1, 255, cv2.FILLED)  # type: ignore
    return output


def show_mask(mask: np.ndarray, axes: Any, random_color: bool = False) -> None:
    """
    Display the mask.

    Args:
        mask (np.ndarray): Input mask image.
        ax (Any): Matplotlib axis to display the mask.
        random_color (bool, optional): Whether to display mask with random color. Defaults to False.
    """
    if random_color:
        color = np.concatenate([np.random.random(3), np.array([0.6])], axis=0)
    else:
        color = np.array([30 / 255, 144 / 255, 255 / 255, 0.6])
    height, width = mask.shape[-2:]
    mask_image = mask.reshape(height, width, 1) * color.reshape(1, 1, -1)
    axes.imshow(mask_image)


def show_points(
    coords: np.ndarray, labels: np.ndarray, axes: Any, marker_size: int = 375
) -> None:
    """
    Display segmentation points.

    Args:
        coords (np.ndarray): Coordinates of points.
        labels (np.ndarray): Labels of points.
        ax (Any): Matplotlib axis to display the points.
        marker_size (int, optional): Size of the marker. Defaults to 375.
    """
    pos_points = coords[labels == 1]
    neg_points = coords[labels == 0]
    axes.scatter(
        pos_points[:, 0],
        pos_points[:, 1],
        color="green",
        marker="*",
        s=marker_size,
        edgecolor="white",
        linewidth=1.25,
    )
    axes.scatter(
        neg_points[:, 0],
        neg_points[:, 1],
        color="red",
        marker="*",
        s=marker_size,
        edgecolor="white",
        linewidth=1.25,
    )


def extract_minimal_masked_image(
    image: np.ndarray, mask: np.ndarray
) -> Tuple[np.ndarray, Tuple[int, int, int, int]]:
    """
    Extract minimal rectangular region from the image based on the mask.

    Args:
        image (np.ndarray): Color image as a NumPy array.
        mask (np.ndarray): Mask image (uint8) as a NumPy array.

    Returns:
        np.ndarray: Minimal rectangular extracted region or None if no non-zero pixels are found.
    """
    if image.shape[:2] != mask.shape:
        raise ValueError("Image and mask must have the same dimensions.")

    non_zero_indices = np.where(mask != 0)

    if len(non_zero_indices[0]) == 0:
        return image, (
            0,
            image.shape[0],
            0,
            image.shape[1],
        )  # No non-zero pixels found in mask

    min_row, max_row = non_zero_indices[0].min(), non_zero_indices[0].max() + 1
    min_col, max_col = non_zero_indices[1].min(), non_zero_indices[1].max() + 1
    crop_region = (min_row, max_row, min_col, max_col)
    masked_image = image[min_row:max_row, min_col:max_col]

    return masked_image, crop_region


def clip_image_margins(image: np.ndarray, clip_percentage: float) -> np.ndarray:
    """
    Clip a specified percentage from all sides of a color image.

    Args:
        image (np.ndarray): Color image as a NumPy array.
        clip_percentage (float): Percentage (0.0 to 1.0) to clip from each side.

    Returns:
        np.ndarray: Color image with clipped margins.
    """
    if not 0 <= clip_percentage <= 1.0:
        raise ValueError("clip_percentage must be between 0.0 and 1.0")

    height, width = image.shape[:2]
    clip_amount = int(clip_percentage * min(height, width) // 2)

    return image[clip_amount:-clip_amount, clip_amount:-clip_amount]


def show_image(
    image: np.ndarray,
    channel_order: str = "BGR",
) -> None:
    """
    Display an image using OpenCV's imshow function.

    Args:
        image (np.ndarray): Image to be displayed as a NumPy array.
    """
    if channel_order == "RGB":
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

    cv2.imshow("Image", image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def is_array_empty(arr: np.ndarray) -> bool:
    """
    Check if the given NumPy array is None or empty.

    Parameters:
    arr (np.ndarray): The NumPy array to be checked.

    Returns:
    bool: True if the array is None or empty, False otherwise.
    """
    return arr is None or arr.size == 0

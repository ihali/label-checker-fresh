# pylint: disable=line-too-long
""" This project provides labelchecker capabilities. """
__version__ = "0.1.0"

from .defaultconfig import DEFAULT_CONFIG_STR  # noqa: F401
from .dataclass import LabelStatus  # noqa: F401
from .labelchecker import LabelChecker, configurations  # noqa: F401

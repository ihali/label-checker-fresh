"""Defines data structures related to label status."""

from typing import Optional
from datetime import datetime
from pydantic import BaseModel


class LabelStatus(BaseModel):
    """
    Represents the status of a label.

    Attributes:
        label_exists (bool): Indicates whether a label exists in the image.
        is_label_correct (Optional[bool]): Indicates whether the label content is correct.
            Defaults to None.
        is_expiry_valid (Optional[bool]): Indicates whether the label expiry is valid.
            Defaults to None.
        expiry_date (Optional[datetime]): The expiry date extracted from the label.
            Defaults to None.
        is_label_angle_valid (Optional[bool]): Indicates whether the label angle relative to the box is valid.
            Defaults to None.
        label_relative_angle (Optional[float]): The relative angle between the label and the box.
            Defaults to None.
        is_label_offset_valid (Optional[bool]): Indicates whether the label is centered on the box or not.
            Defaults to None.
        label_centre_offset (Optional[float]): The offset of the label centre from the box centre in mm.
            Defaults to None.
    """

    label_exists: bool
    is_label_correct: Optional[bool] = None
    is_expiry_valid: Optional[bool] = None
    expiry_date: Optional[datetime] = None
    is_label_angle_valid: Optional[bool] = None
    label_relative_angle: Optional[float] = None
    is_label_offset_valid: Optional[bool] = None
    label_centre_offset: Optional[float] = None

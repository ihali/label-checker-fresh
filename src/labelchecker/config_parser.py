"""Introduces a class to parge configuations"""
import os
import logging
from typing import Union
from pathlib import Path
import tomlkit

from .defaultconfig import DEFAULT_CONFIG_STR

LOGGER = logging.getLogger(__name__)


def configurations(file_path: Union[str, Path]):  # noqa: R0902
    """A function to manage configurations"""

    if os.path.exists(file_path):
        with open(file_path, "r", encoding="utf-8") as file:
            config_data = file.read()
        configs = tomlkit.parse(config_data).unwrap()
    else:
        LOGGER.warning("Can not locate a config file. Loading Default configurations.")
        configs = tomlkit.parse(DEFAULT_CONFIG_STR).unwrap()

    return configs

"""CLI entrypoints for Label Checker"""
import asyncio
import logging
from pathlib import Path
from typing import Any

import click

from labelchecker import __version__
from labelchecker import LabelChecker, DEFAULT_CONFIG_STR

LOGGER = logging.getLogger(__name__)


def dump_default_config(ctx: Any, param: Any, value: bool) -> None:  # pylint: disable=W0613
    """Print the default config and exit"""
    if not value:
        return
    click.echo(DEFAULT_CONFIG_STR)
    if ctx:
        ctx.exit()


@click.command()
@click.version_option(version=__version__)
@click.option(
    "-l",
    "--loglevel",
    help="Python log level, 10=DEBUG, 20=INFO, 30=WARNING, 40=CRITICAL",
    default=30,
)
@click.option(
    "-v", "--verbose", count=True, help="Shorthand for info/debug loglevel (-v/-vv)"
)
@click.option(
    "--defaultconfig",
    is_flag=True,
    callback=dump_default_config,
    expose_value=False,
    is_eager=True,
    help="Show default config",
)
@click.argument("image_path", type=click.Path(exists=True))
def labelchecker_cli(
    image_path: Path,
    loglevel: int = 10,
    verbose: int = 0,
) -> None:
    """Perform check on an image."""
    if verbose == 1:
        loglevel = 20
    if verbose >= 2:
        loglevel = 10
    init_logging(loglevel)
    LOGGER.setLevel(loglevel)

    labelchecker = LabelChecker()
    image = labelchecker.load_image_blocking(image_path)
    label_status, _ = asyncio.run(labelchecker.run_async(image))

    print("-----------------------------------------------")
    print(label_status)
    print("-----------------------------------------------")


def init_logging(loglevel: int) -> None:
    """Initialize logging."""
    logging.basicConfig(level=loglevel)

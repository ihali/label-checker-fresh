"""This module contains the class associated with label detection and validation.
"""
import logging
import asyncio
from pathlib import Path
from typing import Dict, List, Optional, Tuple, Any, Union

import cv2
import numpy as np

from mobile_sam import sam_model_registry, SamPredictor
import advianocrlib as OCRlib
from advianocrlib import DateFormatEnum

from .config_parser import configurations
from .dataclass import LabelStatus
from .utilities import (
    extract_minimal_masked_image,
    morph_close,
    morph_open,
    get_largest_body,
    get_segmentation_seeds_for_box,
    detect_vertical_lines,
    show_points,
    cluster_lines,
    combine_linelets,
    rotate_line_upright_async,
    is_array_empty,
    remove_mask_to_side_of_line,
)

logging.basicConfig(level=logging.INFO)  # Set the logging level as desired
DEBUG = False

if DEBUG:
    import matplotlib.pyplot as plt


class LabelChecker:
    """
    Label checker class.

    Args:
        configs (config, optional): configs for labelchecker configuration.

    Attributes:
        configs (Optional[config]): Configuration for label checker.
        segmentor (Optional[SamPredictor]): Segmentor for for box and label segmentation.
        ocr (OCR): OCR engine for text extraction.
    """

    def __init__(self, config_path: Union[str, Path] = "config.toml") -> None:
        """
        Initialize label checker with configurations.

        Args:
            config_path (Union[str, Path], optional): Path to the configuration file.
        """
        self.configs: Dict = configurations(config_path)
        self.segmentor: SamPredictor = self._initialize_segmentor(self.configs)
        self.ocr: OCRlib.OCR = self._initialize_ocr(self.configs)

        # Dummy run to set gpu overhead settings. Exact fix would be better than this hotfix.
        self.segmentor.set_image(np.zeros((50, 50, 3), dtype=np.uint8))
        self.segmentor.predict(
            point_coords=np.array([[20, 20]]),
            point_labels=[1],
            multimask_output=False,
        )

    @staticmethod
    def _initialize_segmentor(configs: dict) -> SamPredictor:
        """
        Initialize an instance of Segmentor.

        Returns:
            SamPredictor: Initialized segmentor.
        """
        model_type = configs["label_detection"]["model_type"]
        model_path = configs["label_detection"]["model_path"]

        sam = sam_model_registry[model_type](checkpoint=model_path)
        if configs["label_detection"]["use_gpu"]:
            sam.to(device="cuda")
        predictor = SamPredictor(sam)
        return predictor

    @staticmethod
    def _initialize_ocr(configs: dict) -> OCRlib.OCR:
        """
        Initialize an instance of OCR.
        """
        ocr_config = OCRlib.OCRConfigurations()
        ocr_config.update(**configs["ocr_configuration"])
        ocr_predictor: OCRlib.OCR = OCRlib.OCR(ocr_config)
        return ocr_predictor

    async def load_image_async(self, image_path: Union[str, Path]) -> np.ndarray:
        """
        Load image from file path using OpenCV asynchronously.

        Args:
            image_path (str, Path): Path to the image file.

        Returns:
            np.ndarray: Loaded image as a NumPy array.
        """
        loop = asyncio.get_event_loop()
        image = await loop.run_in_executor(None, cv2.imread, str(image_path))
        if image is None:
            raise FileNotFoundError(f"Failed to load image: {image_path}")
        if len(image.shape) == 3:
            image = await loop.run_in_executor(
                None, cv2.cvtColor, image, cv2.COLOR_BGR2RGB
            )
        return image

    def load_image_blocking(self, image_path: Union[str, Path]) -> np.ndarray:
        """
        Load image with a file path.
        """
        return asyncio.run(self.load_image_async(image_path))

    @staticmethod
    async def set_roi_interest_async(image: np.ndarray, roi: List[int]) -> np.ndarray:
        """
        Set Region of Interest (ROI) on the image.

        Args:
            image (np.ndarray): Input image as a NumPy array.
            roi (List): Region of Interest (ROI) coordinates (x, y, width, height).

        Returns:
            np.ndarray: Image with the specified ROI.
        """
        roi_x, roi_y, roi_width, roi_height = roi
        roi_image: np.ndarray = image[
            roi_y : roi_y + roi_height, roi_x : roi_x + roi_width
        ]
        return roi_image

    async def preprocess(self, image: np.ndarray) -> np.ndarray:
        """
        Preprocess the input image based on configured configs.

        Args:
            image (np.ndarray): Input image as a NumPy array.

        Returns:
            np.ndarray: Preprocessed image as a NumPy array.
        """
        loop = asyncio.get_event_loop()

        image_processed: np.ndarray = image.copy()

        if self.configs["preprocessing"]["roi"]:
            image_processed = await self.set_roi_interest_async(
                image_processed, self.configs["preprocessing"]["roi"]
            )

        if self.configs["preprocessing"]["denoise"]:
            scaling_factor = min(image_processed.shape) / 1000.0
            template_h = int(10 * scaling_factor)
            template_window_size = int(7 * scaling_factor)
            template_window_size = int(21 * scaling_factor)
            image_processed = await loop.run_in_executor(
                None,
                cv2.fastNlMeansDenoising,
                image_processed,
                None,
                template_h,
                template_window_size,
                template_window_size,
            )

        if self.configs["preprocessing"]["equalize_histogram"]:
            if len(image_processed.shape) == 3:
                hsv = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)
                image_to_processs = hsv[:, :, 2]

            tile_grid_size = self.configs["preprocessing"]["tile_grid_size"]
            clahe = cv2.createCLAHE(
                clipLimit=2.0, tileGridSize=(tile_grid_size, tile_grid_size)
            )
            image_processed = await loop.run_in_executor(
                None, clahe.apply, image_to_processs
            )

            if len(image_processed.shape) == 3:
                hsv[:, :, 2] = image_processed
                image_processed = cv2.cvtColor(hsv, cv2.COLOR_HSV2RGB)

        return image_processed

    async def run_async(
        self, image_input: Union[np.ndarray, Path]
    ) -> Tuple[LabelStatus, np.ndarray]:  # pylint: disable=too-many-locals
        """
        Run the label checker on the input image asynchronously.

        Args:
            image_input (np.ndarray, Path): Input image as a NumPy array or Path to file.

        Returns:
            tuple: Tuple containing OCR result (list) and original image (np.ndarray).
        """
        image: np.ndarray
        if isinstance(image_input, np.ndarray):
            image = image_input
        else:
            raise ValueError("Invalid image format. Image should be numpy array.")

        height, width = image.shape[0:2]

        label_status: LabelStatus

        pre_processed_image: np.ndarray = await self.preprocess(image)

        box_center = self.configs["box_label_specs"]["box_center_xy_px"]
        box_width, box_height = self.configs["box_label_specs"]["box_dimenstions_xy_px"]

        (
            box_segmentation_points,
            box_segmentation_classes,
        ) = get_segmentation_seeds_for_box(
            (height, width), box_center, box_width, box_height, 250, False
        )

        box_mask = self._segment_box(
            pre_processed_image,
            box_segmentation_points,
            box_segmentation_classes,
        )

        masked_image, crop_region = extract_minimal_masked_image(image, box_mask)

        (
            label_status,
            raw_ocr_data,
            _,
            _,
        ) = await self._validate_label_content(masked_image)

        try:
            (
                label_segmentation_points,
                labels_classes,
            ) = _get_segmentation_seeds_for_label(masked_image, raw_ocr_data)

            label_mask = self._segment_label(
                masked_image,
                label_segmentation_points,
                labels_classes,
            )

            mm_per_px = np.mean(
                (
                    np.array(self.configs["box_label_specs"]["box_size_wh_mm"])
                    / np.array(self.configs["box_label_specs"]["box_dimenstions_xy_px"])
                )
            )
            (
                label_status.is_label_angle_valid,
                label_status.label_relative_angle,
                label_status.is_label_offset_valid,
                label_status.label_centre_offset,
                box_line_img,
                label_line_img,
                box_mask,
                label_mask,
            ) = await _find_and_validate_label_angle_position(
                box_mask,
                label_mask,
                crop_region,
                self.configs["box_label_specs"]["label_tilt_tolerance_deg"],
                self.configs["box_label_specs"]["label_centre_offset_tolerance_mm"],
                mm_per_px,
            )
        except Exception as err:
            logging.warning("Error, bypassing label detection : %s", err)
            label_mask = np.zeros_like(box_mask)
            box_line_img, label_line_img = None, None

        visualized_results = await draw_results_async(
            image,
            crop_region,
            box_mask,
            label_mask,
            box_line_img,
            label_line_img,
            label_status,
        )

        return label_status, visualized_results

    def _segment_box(
        self,
        image: np.ndarray,
        box_segmentation_points: np.ndarray,
        box_segmentation_classes: List[int],
    ) -> np.ndarray:
        """
        Segment the box in the image.

        Args:
            image (np.ndarray): Input image as a NumPy array.
            box_segmentation_points (np.ndarray): Segmentation points for the box.
            box_segmentation_classes (List[int]): Segmentation classes for the box.

        Returns:
            np.ndarray: Mask of the segmented box.
        """

        # Find the box in the image
        if len(image.shape) == 2:
            image = np.stack([image] * 3, axis=-1)

        self.segmentor.set_image(image)
        mask_box, _, _ = self.segmentor.predict(
            point_coords=box_segmentation_points,
            point_labels=box_segmentation_classes,
            multimask_output=False,
        )

        mask_box = np.squeeze(mask_box).astype(np.uint8) * 255
        mask_box = cv2.bitwise_not(mask_box)

        mask_box = morph_close(mask_box)
        mask_box = get_largest_body(mask_box, False)

        # plotting the images
        if DEBUG:
            masked_img = image[:, :, 1] * mask_box
            plt.figure(figsize=(12, 6))
            plt.subplot(231)
            plt.imshow(image)
            plt.title("Image")
            plt.subplot(232)
            plt.imshow(image)
            show_points(
                box_segmentation_points, np.array(box_segmentation_classes), plt.gca()
            )
            plt.title("Seeds")
            plt.subplot(233)
            plt.imshow(mask_box)
            plt.title("Mask")
            plt.subplot(234)
            plt.imshow(masked_img)
            plt.title("Masked Object")
            plt.show()

        return mask_box

    def _segment_label(
        self,
        image: np.ndarray,
        label_segmentation_points: np.ndarray,
        label_segmentation_classes: List[int],
    ) -> np.ndarray:
        """
        Segment the label in the image.

        Args:
            image (np.ndarray): Input image as a NumPy array.
            label_segmentation_points (np.ndarray): Segmentation points for the label.
            label_segmentation_classes (List[int]): Segmentation classes for the label.

        Returns:
            np.ndarray: Mask of the segmented label.
        """
        if len(image.shape) == 2:
            image = np.stack([image] * 3, axis=-1)

        self.segmentor.set_image(image)
        mask_label, _, _ = self.segmentor.predict(
            point_coords=label_segmentation_points,
            point_labels=label_segmentation_classes,
            multimask_output=False,
        )
        mask_label = np.squeeze(mask_label).astype(np.uint8) * 255
        mask_label = morph_open(mask_label)
        mask_label = get_largest_body(mask_label, False)

        # # plotting the images
        if DEBUG:
            masked_img = image[:, :, 0] * mask_label
            plt.figure(figsize=(12, 6))
            plt.subplot(231)
            plt.imshow(image)
            plt.title("Image")
            plt.subplot(232)
            plt.imshow(image)
            show_points(
                label_segmentation_points.astype(int),
                np.array(label_segmentation_classes),
                plt.subplot(232),
            )
            plt.title("Seeds")
            plt.subplot(233)
            plt.imshow(mask_label)
            plt.title("Mask")
            plt.subplot(234)
            plt.imshow(masked_img)
            plt.title("Masked Object")
            plt.show()
        return mask_label

    async def _get_label_content(
        self, image: np.ndarray
    ) -> Tuple[List[str], List[Any], Any]:
        """
        Get the content of the label from the image.

        Args:
            image (np.ndarray): Input image as a NumPy array.

        Returns:
            Text extracted from the label, bounding boxes, and raw OCR result.
        """
        raw_ocr_result, _ = await self.ocr.run_async(image)
        all_text = OCRlib.extract_text(raw_ocr_result)
        bounding_boxes = OCRlib.extract_boundingbox(raw_ocr_result)
        return all_text, bounding_boxes, raw_ocr_result

    async def _validate_label_content(
        self, image: np.ndarray
    ) -> Tuple[LabelStatus, Any, Any, Any]:
        """
        Validate the content of the label extracted from the image asynchronously.

        Args:
            image (np.ndarray): Input image as a NumPy array.

        Returns:
            Tuple[LabelStatus, Any, Any, Any]: Status of label validation, ocr result,
            ocr keyword matches and expiry date result.
        """
        label_status = LabelStatus(label_exists=False)

        texts, _, raw_ocr_result = await self._get_label_content(image)

        # If text is detected on the
        label_status.label_exists = len(texts) > 0

        # Check the correct label is used (match keywords with label data in config)
        keywords = self.configs["box_label_specs"]["keywords"]
        (
            selected_result,
            _,
        ) = await OCRlib.helper_functions.find_matching_text_line_async(
            raw_ocr_result,
            keywords,
            self.configs["ocr_configuration"]["text_match_threshold"],
        )
        keyword_matches = OCRlib.extract_text(selected_result)
        label_status.is_label_correct = len(keyword_matches) == len(keywords)

        # validate expiry
        enum_by_value = {member.value: member for member in DateFormatEnum}
        date_format_enum = enum_by_value.get(
            self.configs["box_label_specs"]["datetime_format"]
        )

        label_expiry_date = await OCRlib.helper_functions.find_date_async(
            ocr_result=raw_ocr_result, date_format=date_format_enum
        )
        config_expiry_date = await OCRlib.helper_functions.find_date_async(
            ocr_result=[([], self.configs["box_label_specs"]["expiry_date"])],
            date_format=date_format_enum,
        )

        if label_expiry_date:
            label_status.is_expiry_valid = (
                label_expiry_date[0][0] <= config_expiry_date[0][0]
            )
            label_status.expiry_date = label_expiry_date[0][0]
        else:
            label_status.expiry_date = None
            logging.warning("Expiry Date not found/read from the box.")

        return label_status, raw_ocr_result, keyword_matches, label_expiry_date


def _get_segmentation_seeds_for_label(
    image: np.ndarray, raw_ocr_data: Any
) -> Tuple[np.ndarray, List[int]]:
    """
    Get segmentation seeds for labeling based on OCR data.

    Args:
        image (np.ndarray): Input image as a NumPy array.
        raw_ocr_data (Any): Raw OCR data.

    Returns:
        Tuple[np.ndarray, List[int]]: Segmentation points and classes for labeling.
    """
    height, width = image.shape[0:2]
    bounding_boxes = OCRlib.extract_boundingbox(raw_ocr_data)

    # -----approach 1-----
    # bounding_boxes_arr=np.array(bounding_boxes)
    # x_coords = bounding_boxes_arr[:,:,0].flatten()
    # y_coords = bounding_boxes_arr[:,:,1].flatten()
    # min_x, max_x = np.min(x_coords), np.max(x_coords)
    # min_y, max_y = np.min(y_coords), np.max(y_coords)

    # label_width = int(max_x - min_x)
    # label_height = int(max_y - min_y)
    # label_center = (int((min_x + max_x) / 2), int((min_y + max_y) / 2))

    # -----approach 2-----
    width_margin = 0.15
    height_margin = 0.15
    bounding_box_centroids = OCRlib.get_boundingbox_centroid(bounding_boxes)
    label_width = int(
        max(bounding_box_centroids[:, 0]) - min(bounding_box_centroids[:, 0])
    )
    label_height = int(
        max(bounding_box_centroids[:, 1]) - min(bounding_box_centroids[:, 1])
    )
    label_width += int(label_width * width_margin)
    label_height += int(label_height * height_margin)

    label_center = (
        int(min(bounding_box_centroids[:, 0]) + label_width / 2),
        int(min(bounding_box_centroids[:, 1]) + label_height / 2),
    )

    points, classes = get_segmentation_seeds_for_box(
        (height, width), label_center, label_width, label_height, 250, True
    )

    indices = [i for i, x in enumerate(classes) if x == 1]

    positive_points = points[indices]
    negative_points = [
        [0.05, 0.05],
        # [0.05, 0.5],
        [0.05, 0.95],
        [0.95, 0.05],
        # [0.95, 0.5],
        [0.95, 0.95],
    ]
    negative_points = np.floor(np.array(negative_points) * np.array([width, height]))
    label_segmentation_points = np.vstack((positive_points, negative_points))
    labels_classes = [1] * len(positive_points) + [0] * len(negative_points)

    return label_segmentation_points, labels_classes


async def _find_and_validate_label_angle_position(
    box_mask: np.ndarray,
    label_mask: np.ndarray,
    box_crop_region: Tuple[int, int, int, int],
    ang_tol: float = 5.0,
    center_tol: float = 20.0,
    mm_per_px: float = 1.0,
) -> Tuple[
    Optional[bool],
    Optional[float],
    Optional[bool],
    Optional[float],
    Optional[np.ndarray],
    Optional[np.ndarray],
    np.ndarray,
    np.ndarray,
]:
    """
    Find and validate the angle of the label relative to the box.

    Returns:
         Flag indicating angle validity,the angle difference, line images.
    """
    is_label_offset_valid: Optional[bool] = None
    label_centre_offset: Optional[float] = None
    label_centre_px: Optional[float] = None
    adjusted_box_centre_px: Optional[float] = None

    label_lines, label_lines_ang, label_line_img = detect_vertical_lines(label_mask)
    box_lines, box_lines_ang, box_line_img = detect_vertical_lines(box_mask)

    if not is_array_empty(box_lines_ang) and not is_array_empty(label_lines_ang):
        angle_difference = np.abs(np.mean(box_lines_ang) - np.mean(label_lines_ang))
        is_relative_angle_valid = angle_difference <= ang_tol
    else:
        if is_array_empty(box_lines_ang):
            logging.warning(
                "Edges of the box were not detected. Failed to find relative angle."
            )
        if is_array_empty(label_lines_ang):
            logging.warning(
                "Edges of the label were not detected. Failed to find relative angle."
            )
        is_relative_angle_valid = None
        angle_difference = None
        is_label_offset_valid = None
        label_centre_offset = None

    (
        left_label_lines,
        right_label_lines,
        left_box_lines,
        right_box_lines,
    ) = await cluster_lines_async(label_lines, box_lines)

    (
        left_label_line,
        right_label_line,
        left_box_line,
        right_box_line,
    ) = await combine_linelets_async(
        left_label_lines, right_label_lines, left_box_lines, right_box_lines
    )

    refined_box_mask = await remove_mask_outside_lines(
        left_box_line, right_box_line, box_mask
    )
    refined_label_mask = await remove_mask_outside_lines(
        left_label_line, right_label_line, label_mask
    )

    upright_lines = await asyncio.gather(
        rotate_line_upright_async(left_label_line),
        rotate_line_upright_async(right_label_line),
        rotate_line_upright_async(left_box_line),
        rotate_line_upright_async(right_box_line),
    )
    (
        left_label_line_upright,
        right_label_line_upright,
        left_box_line_upright,
        right_box_line_upright,
    ) = upright_lines

    if not is_array_empty(left_box_line_upright) and not is_array_empty(
        right_box_line_upright
    ):
        box_centre_px = (
            left_box_line_upright[0]
            + (right_box_line_upright[0] - left_box_line_upright[0]) / 2
        )
        adjusted_box_centre_px = (
            box_centre_px - box_crop_region[2]
        )  # box_crop_region: min_row, max_row, min_col, max_col
    else:
        # FIXME: handle the case if lines were no detected
        box_centre_px = None
        adjusted_box_centre_px = None

    if not is_array_empty(left_label_line_upright) and not is_array_empty(
        right_label_line_upright
    ):
        label_centre_px = (
            left_label_line_upright[0]
            + (right_label_line_upright[0] - left_label_line_upright[0]) / 2
        )
    else:
        left_label_line_upright = None

    if adjusted_box_centre_px is not None and label_centre_px is not None:
        label_centre_offset = (
            np.abs(adjusted_box_centre_px - label_centre_px) * mm_per_px
        )
        is_label_offset_valid = label_centre_offset <= center_tol  # type: ignore

    if DEBUG:
        plt.figure(figsize=(12, 6))
        plt.subplot(121)
        if box_line_img is not None:
            plt.imshow(box_line_img)
        plt.title("box_line_img")
        plt.subplot(122)
        if label_line_img is not None:
            plt.imshow(label_line_img)
        plt.title("label_line_img")
        plt.show()

    return (
        is_relative_angle_valid,
        angle_difference,
        is_label_offset_valid,
        label_centre_offset,
        box_line_img,
        label_line_img,
        refined_box_mask,
        refined_label_mask,
    )  # type: ignore


async def cluster_lines_async(
    label_lines: np.ndarray, box_lines: np.ndarray, n_clusters: int = 2
) -> tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    """Clusters lines asynchronously."""
    task_output = await asyncio.gather(
        cluster_lines(label_lines, n_clusters), cluster_lines(box_lines, n_clusters)
    )
    left_label_lines, right_label_lines = task_output[0]
    left_box_lines, right_box_lines = task_output[1]
    return left_label_lines, right_label_lines, left_box_lines, right_box_lines


async def combine_linelets_async(
    left_label_lines: np.ndarray,
    right_label_lines: np.ndarray,
    left_box_lines: np.ndarray,
    right_box_lines: np.ndarray,
) -> tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    """Combines linelets asynchronously."""
    task_output = await asyncio.gather(
        combine_linelets(left_label_lines, right_label_lines),
        combine_linelets(left_box_lines, right_box_lines),
    )

    left_label_line, right_label_line = task_output[0]
    left_box_line, right_box_line = task_output[1]
    return left_label_line, right_label_line, left_box_line, right_box_line


async def remove_mask_outside_lines(
    left_line: np.ndarray,
    right_line: np.ndarray,
    mask: np.ndarray,
) -> tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    """Remove the mask pixels outside lines."""
    mask_images = await asyncio.gather(
        remove_mask_to_side_of_line(left_line, mask, "left"),
        remove_mask_to_side_of_line(right_line, mask, "right"),
    )
    new_mask = cv2.bitwise_and(mask_images[0], mask_images[1], None)
    return new_mask


async def draw_results_async(
    image: np.ndarray,
    crop_region: Tuple[int, int, int, int],
    box_mask: np.ndarray,
    label_mask: np.ndarray,
    box_line_img: Optional[np.ndarray],
    label_line_img: Optional[np.ndarray],
    label_status: LabelStatus,
) -> np.ndarray:
    """
    Draw results asynchronously on an image.

    Args:
        image (np.ndarray): Input image.
        crop_region (tuple): Region to crop from the image.
        box_mask (np.ndarray): Mask for bounding boxes.
        label_mask (np.ndarray): Mask for labels.
        box_line_img (Optional[np.ndarray]): Image with lines for bounding boxes.
        label_line_img (Optional[np.ndarray]): Image with lines for labels.
        label_status (LabelStatus): Status of the label.

    Returns:
        np.ndarray: Image with drawn results.
    """
    in_image = image.copy()
    if len(image.shape) == 2:
        in_image = cv2.cvtColor(in_image, cv2.COLOR_GRAY2BGR)

    min_row, max_row, min_col, max_col = crop_region
    cropped_image = in_image[min_row:max_row, min_col:max_col]
    cropped_box_mask = box_mask[min_row:max_row, min_col:max_col]
    if box_line_img is not None and np.any(box_line_img):
        cropped_box_line = box_line_img[min_row:max_row, min_col:max_col]
    else:
        cropped_box_line = np.zeros_like(
            label_mask, dtype=np.uint8
        )  # blank images for overlay
        label_line_img = cropped_box_line

    async def create_color_mask(mask, color=(0, 255, 0)):
        mask_3ch = np.stack([mask] * 3, axis=-1)
        non_zero_mask = np.any(
            mask_3ch != 0, axis=-1
        )  # Convert to single-channel boolean mask
        mask_3ch[non_zero_mask] = list(color)
        return mask_3ch

    masks = [
        cropped_box_mask,
        label_mask,
        cropped_box_line,
        label_line_img,
    ]  # List of masks
    colors = [
        (109, 130, 107),
        (200, 167, 200),
        (255, 0, 0),
        (255, 0, 0),
    ]  # List of colors
    tasks = [create_color_mask(mask, color) for mask, color in zip(masks, colors)]
    overlays = await asyncio.gather(*tasks)

    # Combine colored masks
    summed_mask = cropped_image.copy()
    for overlay in overlays:
        non_zeros = np.nonzero(overlay)
        summed_mask[non_zeros] = overlay[non_zeros]

    # show_image(summed_mask)

    # overlay on image
    alpha = 0.8
    overlay_image = cv2.addWeighted(summed_mask, alpha, cropped_image, 1 - alpha, 0)
    # show_image(overlay_image)

    full_image = in_image.copy()
    full_image[min_row:max_row, min_col:max_col] = overlay_image
    # show_image(full_image)

    # put angle text
    expiry_date = (
        label_status.expiry_date.to_date_string()  # type: ignore
        if label_status.expiry_date
        else "None"
    )

    if label_status.label_relative_angle:
        label_relative_angle_rounded = str(round(label_status.label_relative_angle, 2))
    else:
        label_relative_angle_rounded = "None"

    if label_status.label_centre_offset:
        label_centre_offset_rounded = str(round(label_status.label_centre_offset, 2))
    else:
        label_centre_offset_rounded = "None"

    text = (
        f"Label Exists: {label_status.label_exists}\n"
        f"Label Correct: {label_status.is_label_correct}\n"
        f"Expiry Valid: {label_status.is_expiry_valid}\n"
        f"Expiry Date: {expiry_date}\n"
        f"Label Angle Valid: {label_status.is_label_angle_valid}\n"
        f"Label Relative Angle: {label_relative_angle_rounded} deg\n"
        f"Label Centre Offset Valid: {label_status.is_label_offset_valid}\n"
        f"Label Centre Offset: {label_centre_offset_rounded} mm\n"
    )

    # Define the position and font settings for the text
    position = (30, 30)  # You can adjust the position as per your requirement
    font = cv2.FONT_HERSHEY_SIMPLEX
    font_scale = 0.75
    font_color = (125, 125, 255)
    line_type = 2
    lines = text.split("\n")
    text_image = full_image.copy()
    y_positions = [
        position[1] + index * int(1.5 * font_scale * 20) for index in range(len(lines))
    ]

    async def put_text_async(line, y_pos):
        cv2.putText(
            text_image,
            line,
            (position[0], y_pos),
            font,
            font_scale,
            font_color,
            line_type,
        )

    await asyncio.gather(
        *(put_text_async(line, y_pos) for line, y_pos in zip(lines, y_positions))
    )

    return text_image

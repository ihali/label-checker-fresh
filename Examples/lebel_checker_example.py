""" Example usage """
import asyncio
from labelchecker import LabelChecker
from labelchecker.utilities import show_image

labelchecker = LabelChecker()
image = labelchecker.load_image_blocking("./Examples/hetki_brown.jpg")
label_status, visualized_results = asyncio.run(labelchecker.run_async(image))
show_image(visualized_results, "RGB")
print(label_status)
